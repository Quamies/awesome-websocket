const route = require('express').Router()
const Rider = require('../models/rider')
const Driver = require('../models/driver')

//add rider
route.post('/add_rider',async(req,res)=>{
    const newRider = new Rider(req.body.new_rider)
    try{
        const rider = await newRider.save()
        res.status(201).json({
            user:rider
        })
    }catch(e){
        res.send("error saving new rider")
    }
})

//add driver
route.post('/add_driver',async(req,res)=>{
    const newDriver = new Driver(req.body.new_driver)
    try{
        const driver = await newDriver.save()
        res.status(201).json({
            user:driver
        })
    }catch(e){
        res.status(404).json({
            message:"Error saving new driver",
            body:e
        })
    }
})

module.exports = route