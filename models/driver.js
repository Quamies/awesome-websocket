const mongoose = require('mongoose');

const DriverSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    name:{
        type: String,
        required: true
    },
    phone:{
        type: String,
        required: true
    },
    location: {
        type: {
            type: String,
            default: 'Point'
        },
        coordinates: {
            type: [Number],
            required: true
        }
    },
    availability: {
        type: Boolean,
        default: true
    },
    rating: {
        type: Number,
        default: 0
    }
});

DriverSchema.index({ location: '2dsphere' });

const Driver = mongoose.model('Afrocargo_driver', DriverSchema);

module.exports = Driver;
