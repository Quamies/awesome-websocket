const express = require('express')
const {WebSocketServer} = require('ws')
const {Server} = require('socket.io')
const {createServer} = require('http')
const cors = require('cors')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const bodyParser = require('body-parser')

const trip_routes = require('./routes/trip_controller')
const auth = require('./routes/auth')

dotenv.config()

let PORT = 8080;

const app = express()
app.use(cors())
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.json());

const server = createServer(app)
app.use('/api/v1',trip_routes)
app.use('/api/v1/auth',auth)

const wss = new WebSocketServer({ server }, {
    perMessageDeflate: false
  });

wss.on('connection', function connection(ws) {
    //get driver location
    ws.on('message', function message(data) {
      console.log('received: %s', data);
    });
  
    ws.send('Server message');
});

mongoose.connect(process.env.MONGODB_URI,{ useNewUrlParser: true, useUnifiedTopology: true },(err)=>{
  if(err){
    console.log(err)
  }
  console.log("Database connected")
})

server.listen(PORT,()=>{
    console.log(`Server listening on ${PORT}`)
})

