const {model,Schema} = require('mongoose');

const waitingRequestSchema = Schema({
    driver:{
        email: {
            require: true,
            type: String,
            trim: true,
            unique: true,
            lowercase: true,
            
        },
        firstName: {
            require: true,
            type: String,
            trim: true
        },
    },
    lat:{
        type:Number,
        require:true
    },
    lng:{
        type:Number,
        require:true
    },
    address:{
        type:String,
        require:true
    },
    
})

module.exports = model('waitingRequest',waitingRequestSchema)