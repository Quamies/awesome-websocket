const { model, Schema } = require("mongoose");

const RiderSchema = Schema({
  userName: {
    type: String,
  },
  phoneNo: {
    type: String,
  },
  email: {
    type: String,
  },
  rating: {
    type: Number,
    default: 0,
  },
  pickUplocation: {
    type: {
      type: String,
      default: "Point",
    },
    coordinates: {
      type: [Number],
      required: true,
    },
  },
  destination: {
    type: {
      type: String,
      default: "Point",
    },
    coordinates: {
      type: [Number],
      required: true,
    },
  },
});

RiderSchema.index({ pickupLocation: '2dsphere', destination: '2dsphere' })

const rider = model('Afrocargo_rider', RiderSchema);

module.exports = rider;
