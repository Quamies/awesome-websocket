function selectBestDriver(drivers, pickupLocation) {
  // Sort drivers by rating in descending order
  drivers.sort((a, b) => b.rating - a.rating);

  // Calculate the distance from each driver to the pickup location
  drivers.forEach((driver) => {
    driver.distance = getDistanceFromLatLonInKm(
      driver.location.coordinates[1],
      driver.location.coordinates[0],
      pickupLocation.coordinates[1],
      pickupLocation.coordinates[0]
    );
  });

  // Sort drivers by distance in ascending order
  drivers.sort((a, b) => a.distance - b.distance);

  // Return the driver with the highest rating and shortest distance
  return drivers[0];
}

// This function calculates the distance between two locations in kilometers
function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
  var R = 6371; // Radius of the earth in kilometers
  var dLat = deg2rad(lat2 - lat1); // deg2rad below
  var dLon = deg2rad(lon2 - lon1);
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in kilometers
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI / 180);
}

// function selectBestDriver(drivers,pickupLocation) {
//   let bestDriver = null;
//   let bestRating = 0;
//   let bestDistance = Infinity;

//   for (const driver of drivers) {
//     // Skip this driver if they don't have a rating, distance, or latitude/longitude
//     if (
//       !driver.rating ||
//       !driver.location ||
//       !driver.location.coordinates[1] ||
//       !driver.location.coordinates[0]
//     )
//       continue;

//     // Calculate the distance from the driver to the pickup location
//     const driverDistance = Math.sqrt(
//       driver.location.coordinates[1] * driver.location.coordinates[1] +
//         driver.location.coordinates[0]* driver.location.coordinates[0]
//     );
//     bestDistance = driverDistance

//     // If the driver has a higher rating and a smaller distance, they are the new best
//     // if (
//     //   driver.rating > bestRating ||
//     //   (driver.rating === bestRating && driverDistance < bestDistance)
//     // ) {
//     //   bestDriver = driver;
//     //   bestRating = driver.rating;
//     //   bestDistance = driverDistance;
//     // }
//   }

//   return bestDistance;
// }

module.exports = selectBestDriver;
