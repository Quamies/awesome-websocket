const nodemailer = require('nodemailer');

async function sendNotification(driverEmail, message) {
  // create a transporter object using the default SMTP transport
  let testAccount = await nodemailer.createTestAccount();

  let transporter = nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
        user: testAccount.user, // generated ethereal user
        pass: testAccount.pass, // generated ethereal password
    }
  });

  // send the notification email
  let info = await transporter.sendMail({
    from: '"Ride Sharing App" <noreply@example.com>', // sender address
    to: driverEmail, // list of receivers
    subject: 'New ride request', // Subject line
    text: message // plain text body
  });

  console.log('Message sent: %s', info.messageId);
  console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
}

module.exports = sendNotification
