const {model,Schema} = require('mongoose')

const pickupLocationSchema = Schema({
    rider: {
        userName: {
            type: String,
            
          },
          phoneNo: {
            type: String
          },
          email: {
            type: String
          },
      },
      address: {
        type: String
      },
      lat: {
        type: Number
      },
      lng: {
        type: Number
      }
})

module.exports = model('pickuplocation',pickupLocationSchema)