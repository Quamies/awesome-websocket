const { model, Schema } = require("mongoose");

const tripSchema = Schema({
  tripDate: {
    type: Date,
    default:Date.now
  },
  driver: {
    firstName:{
        type:String
    },
    email:{
        type:String
    }
  },
  rider: {
    userName:{
        type:String
    },
    phoneNo:{
        type:String
    },
    email:{
        type:String
    }
  },
  tripStatus: {
    type: String,
    enum: ['Customer Requested', 'Driver Rejected', 'Driver Accepted', 'Trip Started', 'Trip Completed'],
    default: 'Customer Requested'
  },
  pickupLocation: {
    lat:{
        type:Number
    },
    lon:{
        type:Number
    },
    address:{
        type:String
    }
  },
  pickupDistance: {
    lat:{
        type:Number
    },
    lon:{
        type:Number
    },
    address:{
        type:String
    }
  },
  estimatedPickupTime: {
    type: Number
  },
  dropLocation: {
    lat:{
        type:Number
    },
    lon:{
        type:Number
    },
    address:{
        type:String
    }
  },
  dropDistance: {
    type: Number
  },
  estimatedDropTime: {
    type: Number
  },
  tripFare: {
    type: Number
  },
  fareCollected: {
    type: Boolean,
    default:false
  }
});

module.exports = model('trip',tripSchema)
