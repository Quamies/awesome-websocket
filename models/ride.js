const mongoose = require('mongoose');

const RideSchema = new mongoose.Schema({
    driver: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Afrocarco_driver',
        required: true
    },
    rider: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Afrocarco_rider',
        required: true
    },
    pickupLocation: {
        type: {
            type: String,
            default: 'Point'
        },
        coordinates: {
            type: [Number],
            required: true
        }
    },
    destination: {
        type: {
            type: String,
            default: 'Point'
        },
        coordinates: {
            type: [Number],
            required: true
        }
    },
    paymentMethod: {
        type: String,
        required: true
    },
    status: {
        type: String,
        enum: ['pending', 'accepted', 'completed', 'cancelled'],
        default: 'pending'
    }
});

RideSchema.index({ pickupLocation: '2dsphere', destination: '2dsphere' });

const ride = mongoose.model('Ride', RideSchema);

module.exports = ride;
