"use-strict";

const route = require("express").Router();
const selectBestDriver = require("../controllers/selectBestDriver");
const sendNotification = require('../controllers/sendNotification')

const Rider = require("../models/rider");
const Driver = require("../models/driver");
const Ride = require("../models/ride");
const ObjectId = require("mongodb").ObjectId;
const { default: mongoose } = require("mongoose");

route.get("/all_trips", async (req, res) => {
  try {
    const trip = await Ride.find();
    return res.status(200).send(trip);
  } catch (e) {
    return res.status(400).send(e);
  }
});

route.get("/all_riders", async (req, res) => {
  try {
    const riders = await Rider.find();
    return res.status(200).send(riders);
  } catch (error) {
    return res.status(400).send(error);
  }
});

// Get all drivers
route.get("/all_drivers", async (req, res) => {
  try {
    const drivers = await Driver.find();
    return res.status(200).send(drivers);
  } catch (error) {
    return res.status(400).send(error);
  }
});

//delete rider
route.post("/delete_rider", async (req, res) => {
  try {
    const del_rider = await Rider.deleteOne({ _id: req.body.id });
    return res.json({
      body: del_rider,
      message: "Rider deleted",
    });
  } catch (e) {
    return res.status(400).send(e);
  }
});

//delete diver
route.post("/delete_driver", async (req, res) => {
  try {
    const del_driver = await Driver.deleteOne({ _id: req.body.id });
    return res.json({
      body: del_driver,
      message: "Driver deleted",
    });
  } catch (e) {
    return res.status(400).send(e);
  }
});

//Request ride
route.post("/rides", async (req, res) => {
  try {
    // Validate the request data
    const { pickupLocation, destination, paymentMethod } = req.body;
    if (!pickupLocation || !destination || !paymentMethod) {
      return res.status(400).send({ error: "Missing required fields" });
    }

    // Find available drivers in the pickup area
    const drivers = await Driver.find({
      availability: true,
      location: {
        $near: {
          $geometry: pickupLocation,
          $maxDistance: 5000, // 5 km
        },
      },
    });

    if (!drivers.length) {
      return res
        .status(400)
        .send({ error: "No drivers available in your area" });
    }

    // Select the best driver for the ride
    const driver = selectBestDriver(drivers,pickupLocation);

    // Create a new ride request
    const ride = await Ride.create({
      driver: driver._id,
      rider: req.body.rider,
      pickupLocation,
      destination,
      paymentMethod,
      status: "pending",
    });

    // Send a notification to the driver
    sendNotification(driver.email, 'You have a new ride request!');

    // Return the ride details to the rider       
    return res.send({ ride });
  } catch (err) {
    return res
      .status(500)
      .send({ error: "Error requesting ride", message: err });
  }
});

//accept trip
route.post("/rides/accept", async (req, res) => {
  try {
    // Validate the request
    const ride = await Ride.findById(req.body.ride_id);
    if (!ride) {
      return res.status(404).send({ error: "Ride request not found" });
    }
    if (ride.driver.toString() !== req.body.driver_id.toString()) {
      return res
        .status(401)
        .send({ error: "Unauthorized to accept this ride" });
    }

    // Update the ride status to "accepted"
    ride.status = "accepted";
    await ride.save();

    // Send a notification to the rider
    //sendNotification(ride.rider.email, "Your ride has been accepted!");

    // Return the updated ride details
    return res.send({ ride });
  } catch (err) {
    return res.status(500).send({ error: "Error accepting ride request" });
  }
});

//reject trip
route.post("/rides/cancelled", async (req, res) => {
  try {
    // Validate the request
    const ride = await Ride.findById(req.body.ride_id);
    if (!ride) {
      return res.status(404).send({ error: "Ride request not found" });
    }
    if (ride.driver.toString() !== req.body.driver_id.toString()) {
      return res
        .status(401)
        .send({ error: "Unauthorized to reject this ride" });
    }

    // Update the ride status to "cancelled"
    ride.status = "cancelled";
    await ride.save();

    // Send a notification to the rider
    //sendNotification(ride.rider.email, "Your ride has been accepted!");

    // Return the updated ride details
    return res.send({ ride });
  } catch (err) {
    return res.status(500).send({ error: "Error cancelling ride request" });
  }
});

//completed trip
route.post("/rides/completed", async (req, res) => {
  try {
    // Validate the request
    const ride = await Ride.findById(req.body.ride_id);
    if (!ride) {
      return res.status(404).send({ error: "Ride request not found" });
    }
    if (ride.driver.toString() !== req.body.driver_id.toString()) {
      return res
        .status(401)
        .send({ error: "Unauthorized to complete this ride" });
    }

    // Update the ride status to "accepted"
    ride.status = "completed";
    await ride.save();

    // Send a notification to the rider
    //sendNotification(ride.rider.email, "Your ride has been accepted!");

    // Return the updated ride details
    return res.send({ ride });
  } catch (err) {
    return res.status(500).send({ error: "Error completing ride request" });
  }
});

route.post("/drivers/online", async (req, res) => {
  const { longitude, latitude, id } = req.body;
  const objectId = new ObjectId(id);

  Driver.updateOne(
    { _id: id },
    { $set: { availability:true,location:{
      type:"Point",
      coordinates:[longitude,latitude]
    } } },
    (error, result) => {
      if (error) {
        // handle the error
        return res.status(500).send({ error: "Failed to update driver status" });
      }
      // return success message
      return res.send({ message: "Driver is now online" });
    }
  );
});

route.post("/drivers/offline", async (req, res) => {
  const { id } = req.body;

  Driver.updateOne(
    { _id: id },
    { $set: { availability:false} },
    (error, result) => {
      if (error) {
        // handle the error
        return res.status(500).send({ error: "Failed to update driver status" });
      }
      // return success message
      return res.send({ message: "Driver is now offline" });
    }
  );
});

module.exports = route;
