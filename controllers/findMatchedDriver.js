const Driver = require('../models/driver')

async function findMatchedDriver(pickup){
    //get all drivers
    try{
        const res = await Driver.find().where({location:pickup})
        if(res){
            return res
        }
    }catch(e){
        throw e
    }

    //where their location matches that of the riders

    //return the details of the driver

    //else null
}

module.exports = findMatchedDriver